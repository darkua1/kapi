FROM mhart/alpine-node:14.2.0

WORKDIR /app
COPY package.json package-lock.json ./

# If you have native dependencies, you'll need extra tools
# RUN apk add --no-cache make gcc g++ python

RUN npm install --only=production

FROM mhart/alpine-node:14.2.0
WORKDIR /app
COPY --from=0 /app .
COPY . .
CMD ["npm", "run", "prod"]