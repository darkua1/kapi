export default {
  "apiVersion": "serving.knative.dev/v1",
  "kind": "Service",
  "metadata": {
    "name": "{{serviceName}}",
    "namespace" : "{{projectId}}"
  },
  "spec" : {
    "template": {
      "spec": {
        "containerConcurrency": 80,
        "timeoutSeconds": 900,
        "containers": [
          {
            "image": "{{serviceImage}}"
          }
        ]
      }
    },
    "traffic": [ 
      { 
        "percent": 100,
        "latestRevision": true } 
    ]
  }
}