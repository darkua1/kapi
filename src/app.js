//envs
const KEY_FILE_PATH = "priv_key.json"
const SERVICE_NAME = process.env.SERVICE_NAME || "plotty"
const SERVICE_IMAGE= process.env.SERVICE_IMAGE || "gcr.io/kyso-279217/plotty"
const REGION= process.env.REGION || "us-central1"


import express from 'express'
import {GoogleCloudAPI} from './googleCloudClient.js'
const app = express()
const port = process.env.PORT
const cloud = new GoogleCloudAPI(KEY_FILE_PATH,REGION)

app.post('/start', async (req, res) => {
  
  //check current status
  try {
    const service = await cloud.createService(SERVICE_NAME,SERVICE_IMAGE)
    res.json({status:"starting"})
  }catch(err) {
    console.log('err', err)
    res.json({"status":"error", error: err.response.data.error.message})
  }
});

app.post('/status', async (req, res) => {
  
  //check current status
  try {
    const service = await cloud.getService(SERVICE_NAME)
    const status = (service.status.url)?"running":"starting"; //#todo likely there are better ways to check for this
    res.json({status:status, url:service.status.url})

  }catch(err) {
    console.log('err', err)
    res.json({"status": "off"})
  }
});


app.post('/stop', async (req, res) => {
  try {
    await cloud.deleteService(SERVICE_NAME)
    res.json({status:"stopped"})
  }catch(err) {
    console.log('err', err)
    res.json({"status":"error", error: err.response.data.error.message})
  }
});


app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))