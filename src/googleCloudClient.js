
//predefined in 
import axios from "axios"
import {createRequire} from 'module';
const require = createRequire(import.meta.url);
const {google} = require('googleapis') // googleapis does not support ESM

import template from './service_tmp.js'

const renderServiceTemplate = (name, image, projectId)=>{
  const stringService = JSON.stringify(template)
  .replace("{{serviceName}}",name)
  .replace("{{serviceImage}}",image)
  .replace("{{projectId}}",projectId)

  return JSON.parse(stringService)
}

export class GoogleCloudAPI {
  constructor(keyFilePath, region){
    
    this.auth = new google.auth.GoogleAuth({
      keyFile: keyFilePath,
      scopes: ['https://www.googleapis.com/auth/cloud-platform']
    });
    this.region = region;
  }

  async authenticate(){
    const accessToken =  await this.auth.getAccessToken()
    this.config = {
      headers : {
        "Content-Type" : "application/json",
        "Authorization" : `Bearer ${accessToken}`
      }
    }
    this.projectId = await this.auth.getProjectId();
  }
  
  async setIamPermission(serviceName){
    const permission = {"policy": {"bindings": [{"members": ["allUsers"], "role": "roles/run.invoker"}]}}  
    this.url = `https://run.googleapis.com/v1/projects/${this.projectId}/locations/${this.region}/services/${serviceName}:setIamPolicy?alt=json`
    await axios.post(this.url, permission, this.config)
  }

  async createService(serviceName, serviceImage){
    await this.authenticate()
    const service = renderServiceTemplate(serviceName,serviceImage,this.projectId)
    this.url = `https://us-central1-run.googleapis.com//apis/serving.knative.dev/v1/namespaces/${this.projectId}/services/`
    const r = await axios.post(this.url, service, this.config)
    await this.setIamPermission(serviceName)
    return r.data
  }
  
  async getService(serviceName) {
    await this.authenticate()
    this.url = `https://us-central1-run.googleapis.com//apis/serving.knative.dev/v1/namespaces/${this.projectId}/services/${serviceName}`
    const r = await axios.get(this.url, this.config)
    return r.data
  }

  async deleteService(serviceName) {
    await this.authenticate()
    this.url = `https://us-central1-run.googleapis.com//apis/serving.knative.dev/v1/namespaces/${this.projectId}/services/${serviceName}`
    const r = await axios.delete(this.url, this.config)
    return r.data
  }
}