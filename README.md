# kapi

Goal: create a rest api server that accepts posts requests to run a stateless docker container in the cloud.

### Implementation details

- Google Cloud managed Kubernetes infrastructure, using the cloud run API

- cloud run googleapi is not available for nodejs, so i debug gcloud console in order to implement a simple client for the required API calls

### Requirements
  - setup cloud run https://cloud.google.com/run/docs/setup
  
  - create a service account for server to server authenthication and select as ROLE Cloud Run Admin  https://console.cloud.google.com/apis/credentials/serviceaccountkey Download private key in json and copy to project /priv_key.json
  
  - Build and deploy the plotty image in google registry. ATTENTION: port was hardcoded in code, what does not work in cloud run, please changed it to read from os.environ.get("PORT")


### Run it locally
  
  ```npm install```
  
  ```npm run dev```
  
  with docker image:
  
  ```docker build -t kapi .```
  
  ```docker run -it -e PORT=7777 -e SERVICE_NAME=plotty -e SERVICE_IMAGE=gcr.io/kyso-279217/plotty -e REGION=us-central1 -p 7777:7777 kapi```

### Run it in the cloud

ideally connecting it to a CI/CD pipeline, but for better understanding lets do it manually, since actually what the api does
it all this process in a programmatic way

Build container and publish on google registry (cloud run seems not working with external docker registry)
  
  - get project id

  ```gcloud config get-value project```

  - build image

  ```docker build -t kapi .```

  ```docker tag kapi gcr.io/PROJECT-ID/kapi```

  - push image

  ```gcloud auth configure-docker```

  ```docker push gcr.io/PROJECT-ID/kapi```

  - deploy to cloud run

  ```gcloud run deploy --image gcr.io/kyso-279217/kapi --platform managed --region us-central1 --set-env-vars=SERVICE_NAME=plotty,SERVICE_IMAGE=gcr.io/kyso-279217/plotty,REGION=us-central1```

  Please say yes to Allow unauthenticated, so we can run commands without authentication against api.
  The url that is returned is the url of our api.

### Demo
  In order to demo easily the api, deploying a new plotty container, run

  ```curl -X POST https://kapi-tuew5h2voa-uc.a.run.app/start```

  ```curl -X POST https://kapi-tuew5h2voa-uc.a.run.app/status```

  ```curl -X POST https://kapi-tuew5h2voa-uc.a.run.app/stop```